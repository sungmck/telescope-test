import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../service/api.service';
import { Users } from '../../model/users';

@Component({
  selector: 'home-table',
  templateUrl: './userTable.component.html',
  providers: [ ApiService ],
  styleUrls: ['./userTable.component.css']
})
export class UserTableComponent implements OnInit {

  @Input() loadTable: boolean;
  displayedColumns: string[] = [];
  users: Users[] = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

   ngOnChanges() {
     if (this.loadTable) {
       this.getUsers();
     }
   }

  getUsers(): void {
    this.displayedColumns = ['id', 'username', 'name', 'website'];
    this.apiService.getUsers()
      .subscribe(users => this.users = users);
  }

}
