import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Users } from '../model/users';

const apiUrl = "https://jsonplaceholder.typicode.com/users";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }

  getUsers (): Observable<Users[]> {
    return this.http.get<Users[]>(apiUrl)
      .pipe(
        catchError(this.handleError('getUsers', []))
      );
}

}
